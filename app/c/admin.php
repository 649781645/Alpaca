<?php
class admin extends base{
  function __construct(){
    parent::__construct();
    if(!$this->u['id']){
      redirect(BASE.'login/?&rtu='.BASE.'admin/',"请先登录");
    }    
    if($this->u['level'] < 10 ) redirect(BASE,'权限不够');
    $this->m = load('m/node_m');
  }
  
  function index()
  {
    redirect("?/admin/page/");
  }
  
  function page($upid = '0' ,$id = 0 )
  {
    switch($upid){
      case 'edit':
        $this->edit($id);
        break;
      case 'add':
        $this->add($id);
        break;
      case 'del':
        $this->del($id);
        break;
      default:
        $this->plist($upid);
    }
  }
   
  function plist( $upid = 0 )
  {
    if(isset($_POST['order_by'])){
      foreach($_POST['order_by'] as $k => $v){
        $this->m->update($k, array('order_by'=> $v ));
      }
    }
    $param['upid'] = $upid;
    $param['uper'] = $this->m->get($upid);
    $tot = $this->m->count( " and `type` = 'page' and upid = '$upid' " );
    $psize = 30;
    $pcurrent = isset( $_GET['p'] )? $_GET['p']:0;
    $param['pagination'] = pagination($tot , $pcurrent , $psize ,'/admin/page/index/'.$rel_id.'/?p=');
    $param['records'] = load('m/node_m')->get( " and `type` = 'page' and upid = '$upid' order by `order`,`id` asc" , $pcurrent ,  $psize);
    $this->display('v/admin/index',$param);
  }
  
  function order_edit($id, $type = '')
  {
    $pnode = $this->m->get($id);
    if($type == 'down') {
      $qnode = $this->m->db->query("select id,`order` from node where `order`>$pnode[order] and `type` = 'page' and upid=$pnode[upid] order by `order` asc limit 1");
      $this->m->update($id, array('order' => $qnode[0]['order']));
      $this->m->update($qnode[0]['id'], array('order' => $pnode['order']));
    } else if($type == 'up') {
      $qnode = $this->m->db->query("select id,`order` from node where `order`<$pnode[order] and `type` = 'page' and upid=$pnode[upid] order by `order` desc limit 1");
      $this->m->update($id, array('order' => $qnode[0]['order']));
      $this->m->update($qnode[0]['id'], array('order' => $pnode['order']));
    }
  }
  
  function add( $upid = 0)
  {
    $param['layouts'] = $this->get_layout_list();
    if($_POST['name']){
      $_POST['update_date'] = $_POST['create_date'] = time();
      $_POST['ext1'] = _encode($_POST['ext1']);
      $max_order = $this->m->db->query("select max(`order`) as max_order from node");
      $_POST['order'] = $max_order[0]['max_order'] + 1;
      $this->m->add();
      redirect("?/admin/page/$upid/");
    }
    $param['exts'] = array(
        'meta_keywords'=>'meta_keywords',
        'meta_description'=>'meta_description',
        'page_title'=>'page_title',
        'template'=>'风格文件',
        'layout'=>'排版文件'
      );
    $param['page']['upid'] = $upid;
    $this->display('v/admin/add',$param);    
  }
  
  function del($id) 
  {
    $this->m->del($id);
    echo '1';
    exit;
  }
    
  function get_layout_list()
  {
    $layouts = glob(APP.'v/layout/*.php');
    foreach($layouts as $r){
      $ret[] = str_replace('.php','',basename($r));
    }
    return $ret;
  }
  
  function edit( $id = 0)
  {
    $param['layouts'] = $this->get_layout_list();
    
    if($_POST['name']){
      $_POST['update_date'] = time();
      $_POST['ext1'] = _encode($_POST['ext1']);
      $this->m->update($id);
      redirect("?/admin/page/".$_POST['upid'].'/');
    }
    
    $param['page'] = $page = $this->m->get($id);  
    $param['upid'] = $page['upid'];
    $param['uper'] = $this->m->get($page['upid']);
    $param['ext1'] = _decode($page['ext1']);
    $param['exts'] = array(
        'meta_keywords'=>'meta_keywords',
        'meta_description'=>'meta_description',
        'page_title'=>'page_title',
      );
    $this->display('v/admin/add',$param);    
  }
   
  function link($upid = 'list' ,$id = 0 )
  {
    switch($upid){
      case 'edit':
        $this->linkedit($id);
        break;
      case 'add':
        $this->linkadd($id);
        break;
      case 'del':
        $this->linkdel($id);
        break;
      default:
        $this->linklist($upid);
    }
  }
  
  function linklist($upid)
  {
    if(isset($_POST['order_by'])){
      foreach($_POST['order_by'] as $k => $v){
        $this->m->update($k, array('order_by'=> $v ));
      }
    }
    $param['rel_id'] = $rel_id;
    $tot = $this->m->count( " and `type` = 'link' " );
    $psize = 30;
    $pcurrent = isset( $_GET['p'] )? $_GET['p']:0;
    $param['pagination'] = pagination($tot , $pcurrent , $psize ,'/admin/link/index/'.$rel_id.'/?p=');
    $param['records'] = load('m/node_m')->get( " and `type` = 'link' " , $pcurrent ,  $psize);
    $this->display('v/admin/link',$param);
  }
  
  function linkadd()
  {
    if($_POST['name']){
      $_POST['update_date'] = $_POST['create_date'] = time();
      foreach($_POST['info'] as $info){
        if($info['title'])$ext[] = $info;
      }
      $_POST['ext'] = _encode($ext);
      $this->m->add();
      redirect("?/admin/link/");
    }
    $this->display('v/admin/linkadd',$param);
  }
  
  function linkdel($id)
  {
    $this->m->del($id);
    echo '1';
    exit;
  }
    
  function linkedit( $id = 0)
  {
    if($_POST['name']){
      $_POST['update_date'] = time();
      foreach($_POST['info'] as $info){
        if($info['title'])$ext[] = $info;
      }
      //print_r($ext);
      $_POST['ext'] = _encode($ext);
      //echo $_POST['ext'];
      $this->m->update($id);
      redirect("?/admin/link/");
    }
    
    $param['page'] = $page = $this->m->get($id);
    $param['ext'] = _decode($page['ext']);
    $this->display('v/admin/linkadd',$param);    
  }
  
  function layout()
  {
    $records = glob(APP.'v/layout/*.php');
    foreach($records as $r){
      $nr['size'] = filesize($r);
      $nr['name'] = basename($r);
      $nr['time'] = filemtime($r);
     $nnr[] = $nr;
    }
    $this->display('v/admin/layout',array('records'=>$nnr));    
  }
  
  function layout_edit($layout = '') 
  {
    $layout_file = APP.'v/layout/'.$layout;
    if(file_exists($layout_file)) {
      $param['layout'] = $layout;
      $param['content'] = file_get_contents($layout_file);
      $param['writable'] = is_writable($layout_file);
      $this->display('v/admin/layout_edit', $param);
    } else {
      redirect($_SERVER['HTTP_REFERER'], '该模版文件不存在', '', 3);
    }
  }
  
  function layout_del($layout = '')
  {
    $layout_file = APP.'v/layout/'.$layout;
    if(unlink($layout_file)) {
      echo '1';
    } else {
      echo '删除失败';
    }
  }
  
  function layout_save($layout = '') 
  {
    $layout_file = APP.'v/layout/'.$layout;
    if(file_exists($layout_file)) {
      $content = htmlspecialchars_decode($_POST['content']);
      file_put_contents($layout_file, $content);
      echo '1';
    } else {
      echo '编辑失败';
    }
  }
  
  function redirect($info = '') 
  {
    $info = urldecode($info);
    redirect($_SERVER['HTTP_REFERER'], $info);
  }
  
  function tag()
  {
    if($_POST['info']){
      foreach($_POST['info'] as $info){
        if($info['name'])$ext[] = $info;
      }
      $_POST['ext'] = _encode($ext);
      $tags = $this->m->n('tags');
      if(!empty($tags)) {
        $this->m->update($tags['id']);
      } else {
        $_POST['name'] = 'tags';
        $_POST['type'] = 'tag';
        $_POST['create_date'] = $_POST['update_date'] = time();
        $this->m->add();
      }
      redirect("?/admin/tag/");
    }
    
    $tags = $this->m->n('tags');
    $param['ext'] = _decode($tags['ext']);
    
    $this->display('v/admin/tag',$param);    
  }
  
  function display($view='v/index',$param = array()){
    $param['menu'] =  array(
      'page'=>'页面',
      'link'=>'链接',
      'tag'=>'标签',
      'layout'=>'模板',
      'link'=>'链接',
      'tag'=>'标签',
      'logout'=>'退出',
    );
    $param['al_content'] = view($view,$param,TRUE);
    header("Content-type: text/html; charset=utf-8");
    view('v/admin/template',$param);
  }
}
