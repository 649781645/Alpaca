<div id="slides" class="sp-inner clearfix">
        

<div class="custom">
	<div class="title_bar">
	<h2><?=$title?></h2>
	<span>LEADINDG PRIVATE EQUITY INVESTMENT MANAGEMENT SYSTEM</span></div>
</div>
      
      </div>
<div class="sp-wrap main-bg clearfix">
      
    <!--Module Position user1 to user4-->
      
    	<div id="sp-leftcol" class="clearfix">
		<div id="sp-left" class="clearfix" style="height: 978px;"><div class="sp-inner clearfix">	<div class="module">	
		<div class="mod-wrapper clearfix">		
					<div class="mod-content clearfix">	
				<div class="mod-inner clearfix">
					
<ul class="menu left_menu">
<li class="item-676 current active"><a href="/index.php/slide">公司介绍</a></li><li class="item-688"><a href="/index.php/2012-07-30-01-49-47">加入我们</a></li><li class="item-689"><a href="/index.php/2012-07-30-01-50-11">联系我们</a></li><li class="item-739"><a href="/index.php/2013-03-18-02-09-52">免费试用</a></li><li class="item-717"><a href="/index.php/2012-10-16-09-33-31">媒体纷说</a></li></ul>
				</div>
			</div>
		</div>
	</div>
	<div class="gap"></div>
	</div></div>	</div>
	<div id="sp-maincol" class="clearfix">
		<div class="clr"></div>
		<div id="inner_content" class="clearfix"> <!--Component Area-->
				
					<div class="sp-inner clearfix">
								
<div id="system-message-container">
</div>
				<div class="item-page">
          <?=$content?> 
	<h2>
			公司介绍		</h2>






<p class="rk_p">
	仁科互动（北京）信息科技有限公司是一家创新型互联网商务软件服务商。公司致力于将成熟互联网技术（社交网络，移动，云）同企业业务应用结合，为企业用户提供更好用，专业且低成本的软件服务。</p>
<p class="rk_p">
	公司创始团队成员来自SAP、搜狐和华为等公司, 具有多年管理软件，互联网和销售管理领域经验。多年来，我们深刻地体会到企业在购买昂贵、僵化且复杂难用的传统管理软件后，却因为用户不愿意使用等原因而带不来预期价值的普遍情况，传统<a href="http://www.ingageapp.com" title="CRM" style="color:#000000">CRM</a>居高不下的失败率便是这种状况的典型体现。</p>
<p class="rk_p">
	过去10多年间互联网技术的成熟，为改变这种状况提供了基础。通过互联网来交付软件（SaaS 或云模式）的方式让软件服务的总体成本和风险大幅降低，社交网络让企业应用由僵化的流程为中心向以人为中心过度成为可能，而移动互联网则为企业用户带来消费互联网般便捷、实用和易用的极致体验。</p>
<p class="rk_p">
	正是基于这些行业经验和技术基础，我们推出核心产品：销售易， 一款以支撑销售人员打单、办公为目的的全新型<a href="http://www.ingageapp.com" title="CRM" style="color:#000000">CRM</a>（客户关系管理）：帮助销售人员随时随地获取客户信息，查阅知识文档，跟进销售机会，并同团队高效沟通协作，从而提升销售团队效率和业绩。</p>
<p class="rk_p" style="border-bottom: #ddd 1px solid; padding-bottom: 20px; margin-bottom: 40px">
	无需任何软硬件成本投入，通过网络注册，您和您的团队就可以立即开始轻松管理客户关系，体验团队协作的力量， 增强客户关系，带来销售业绩的提升。</p>
<h5 class="rk_h5">
	公司使命：</h5>
<p class="rk_p" style="border-bottom: #ddd 1px solid; padding-bottom: 20px; margin-bottom: 40px">
	我们的使命是让中国所有企业都能用上好用、专业的软件服务，提升运营效率和竞争力。</p>
<h5 class="rk_h5">
	这些是指引我们的原则和核心价值：</h5>
<div style="margin-top: 30px">
	<div class="value_content">
		<div class="value_left">
			<h5 class="ico_01">
				客户</h5>
			<p class="rk_p">
				为客户创造超越预期的价值，是我们存在的目的。我们的创新和决策来自于倾听客户，深入了解客户需求。</p>
		</div>
	</div>
	<div class="value_content">
		<div class="value_middle">
			<h5 class="ico_02">
				员工</h5>
			<p class="rk_p">
				人是我们的一切。专业，开心的员工是我们确保客户得到最优质服务的前提。</p>
		</div>
	</div>
	<div class="value_content">
		<div class="value_right">
			<h5 class="ico_03">
				诚信</h5>
			<p class="rk_p">
				信任是企业和个人最大的资产。 我们行为和决策应以获取客户、员工， 投资人的信任为准则。尊重，平等，开放，正直是信任滋生的文化氛围。</p>
		</div>
	</div>
	<div class="value_content">
		<div class="value_left">
			<h5 class="ico_04">
				数据安全和隐私</h5>
			<p class="rk_p">
				我们将以最高的规格和措施来保护客户数据安全和隐私。这是我们生存发展的关键。</p>
		</div>
	</div>
	<div class="value_content">
		<div class="value_middle">
			<h5 class="ico_05">
				简洁易用</h5>
			<p class="rk_p">
				软件以人为本。为使用者提供最佳体验和价值，是我们产品设计的基本原则。</p>
		</div>
	</div>
	<div class="value_content">
		<div class="value_right">
			<h5 class="ico_06">
				创新</h5>
			<p class="rk_p">
				倾听客户和市场声音，勇于突破传统技术和运营常规，为客户提供最高效、最有价值的服务， 并让客户参与到公司价值创造过程。</p>
		</div>
	</div>
</div>
<p>
	


</p><div class="custom">
	<p>
	<a class="startup_button" href="/index.php/2013-03-18-02-09-52" title="免费使用"><span>免费试用</span></a></p>
</div>
<p></p>

	
</div>
			</div>
					</div>
		<div class="clr"></div>
	<div id="sp-user-bottom" class="clearfix"><div class="sp-inner clearfix">	<div class="module">	
		<div class="mod-wrapper clearfix">		
					<div class="mod-content clearfix">	
				<div class="mod-inner clearfix">
					

<div class="custom">
	<p>
	<a href="#" id="topofpage" rel="nofollow">返回顶部 </a></p>
</div>
				</div>
			</div>
		</div>
	</div>
	<div class="gap"></div>
	</div></div></div>    </div> 

