<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-cn" lang="zh-cn" >
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" >
  <base href="<?=BASE?>" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="keywords" content="<?=$meta_keywords?$meta_keywords:$title?>" />
  <meta name="description" content="<?=$meta_description?$meta_description:$title?>" />
  <meta name="generator" content="Alpaca! - Open Source Content Management" />
  <title><?=$page_title?$page_title:$title?> - PEPM</title>
  <link href="static/fav.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <link href="static/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="static/pepm.css" type="text/css" />
  <script src="static/js/jquery.js"></script>
  <script src="static/js/bootstrap.min.js"></script>
</head>
<body class="bg clearfix">
<div id="header" class="clearfix">
  <a id="logo" href="./" title="PEPM-移动PEPM投资管理系统(免费试用)-江苏智库PEPM系统"></a>
  <div class="clr"></div>
  <div id="hornav" class="clearfix">
    <ul class="sp-menu level-0">
      <?=alpa('mainmenu','mainmenu')?>
    </ul>
  </div>
  <div class="service_call">400-626-5464</div>
</div>    
  
<?=$al_content?>

<!--Module Position bottom1 to bottom6-->
<div id="sp-bottom" class="clearfix">
<div class="sp-inner">
<div style="width:16.667%" class="sp-block  separator"><div id="bottom1" class="mod-block equal first">	<div class="module">
<div class="mod-wrapper-flat clearfix">
<h3 class="header"><span>PEPM</span></h3>
                    
<ul class="menu">
  <?=alpa('pepm')?>
</ul>
  </div>
</div>
</div></div><div style="width:16.667%" class="sp-block  separator"><div id="bottom3" class="mod-block equal">	<div class="module">	
  <div class="mod-wrapper-flat clearfix">		
          <h3 class="header">			
      <span>关于我们</span>				</h3>
                  
<ul class="menu">

<?=alpa('about')?></ul>
  </div>
</div>
</div></div><div style="width:16.667%" class="sp-block  separator"><div id="bottom4" class="mod-block equal">	<div class="module">	
  <div class="mod-wrapper-flat clearfix">		
          <h3 class="header">			
      <span>选择我们</span>				</h3>
                  
<ul class="menu">
<?=alpa('whyus')?>
</ul>
  </div>
</div>
</div></div>
<div style="width:16.667%" class="sp-block  separator"><div id="bottom5" class="mod-block equal">	<div class="module">	
<div class="mod-wrapper-flat clearfix">		
<h3 class="header"><span>友情链接</span></h3>
<ul class="weblinks"> 
<li><a target=_blank href="http://www.ceocio.com.cn/" >IT经理世界</a>	</li>
<li><a target=_blank href="http://www.chinaventure.com.cn/" >投中集团</a>	</li>
<li><a target=_blank href="http://www.pedaily.cn/" >投资界</a></li>
<li><a target=_blank href="http://www.cyzone.cn/" >《创业邦》</a></li>
<li><a target=_blank href="http://www.vc800.com/" >创800</a>	
</li>
</ul>
  </div>
</div>
</div></div><div style="width:16.667%" class="sp-block "><div id="bottom6" class="mod-block equal last">	<div class="module">	
  <div class="mod-wrapper-flat clearfix">		
          <h3 class="header">			
      <span>联系我们</span>				</h3>
                  
<div class="custom"  >
<div style="padding-left: 5px;">
<p style="color:#ddd;">
  <span>服务热线：400-626-5464</span><br />
  <span>Email : x@pepm.com.cn</span><br />
  <span>Web : www.pepm.com.cn</span></p>
</div>
</div>
  </div>
</div>
</div></div>        </div>
    </div>
      </div>
  
  <!--Footer-->
  <div id="sp-footer" class="clearfix">
    <div class="sp-wrap">
      <div class="sp-inner">
        

        <div class="cp">
          
Copyright &copy; 2011-2013 江苏智库信息科技有限公司            
        </div>
                    <div id="footer-nav">
            
<ul class="menu">
<li class="item-464"><span class="separator">苏ICP备13010943号</span></li>
</ul>
          </div>
                
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" src="http://tajs.qq.com/stats?sId=26972501" charset="UTF-8"></script>
</body>
</html>
