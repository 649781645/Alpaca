  <!--Module Position slides-->  
  <div id="slides" class="sp-inner clearfix">
    <div style="clear: both;"></div>
    <div class="box_skitter mod_btslideshow" >
      <div id="myCarousel" class="carousel slide" style="width:980px;margin:auto;">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class=""></li>
          <li data-target="#myCarousel" data-slide-to="1" class=""></li>
          <li data-target="#myCarousel" data-slide-to="2" class="active"></li>
        </ol>
        <div class="carousel-inner">
          <div class="item">
            <img src="static/img/bootstrap-mdo-sfmoma-01.jpg" alt="">
            <div class="carousel-caption">
              <h4>轻松实现移动办公</h4>
              <p>
                系统可完美应用于计算机、iPad、iPhone、Android 等多种智能终端。无论是机场、酒店或者办公室，你都能对项目进展一手掌握。
              </p>
            </div>
          </div>
          <div class="item">
            <img src="static/img/bootstrap-mdo-sfmoma-02.jpg" alt="">
            <div class="carousel-caption">
              <h4>私募股权投资管理</h4>
              <p>适用于 VC、PE以及机构LP使用的协同办公系统。实现了案源、投资组合、基金、LP的管理，方便跨地域、跨平台(电脑、手机、iPad)进行数据共享。 并提供行业特有的公允价值、现金流、 IRR 测算等功能， 灵活的字段配置以及和分析功能。</p>
            </div>
          </div>
          <div class="item active">
            <img src="static/img/bootstrap-mdo-sfmoma-03.jpg" alt="">
            <div class="carousel-caption">
              <h4>简单便是美</h4>
              <p>根据私募股权投资业务和投资后管理的实际需要，简化操作流程，聚焦核心价值。最大限度的降低软件的学习成本，力求实现上手即用。</p>
            </div>
          </div>
        </div>
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
      </div>
      <div style="clear: both;"></div>

    </div>
  </div>  
      
  <div class="sp-wrap main-bg clearfix">
    <!--Module Position user1 to user4-->

  
  
    <div id="sp-maincol" class="clearfix" style="width:70%;">
      <div id="sp-slide1" class="clearfix">
        <div class="sp-inner clearfix">
          <div class="custom"  >
            <div class="clear" style="margin: 0 0 10px 0; padding-top: 30px;">
            <div class="content_d">
              <h4 class="intr_01">
                专业的投资管理</h4>
              <p>
                从潜在投资管理到投资项目再到分析和控制，PEPM通过自动化投资流程来提升团队执行力, 实现投资业绩持续增长。这一切，在手机上更加容易。</p>
              <a href="p/explore/">了解详细&gt;&gt;</a></div>
            <div class="content_d">
              <h4 class="intr_02">
                全新的沟通协作</h4>
              <p>
                将投资，同事, 项目装在&ldquo;口袋&rdquo;里，用类微信的语音沟通和类微博的信息传播,让&ldquo;路上战士&rdquo;们快速获取所需援助和信息, PEPM大幅提升&ldquo;作战&rdquo;效率。</p>
              <a href="p/cooperate/">了解详细&gt;&gt;</a></div>
            <div class="content_d">
              <h4 class="intr_03">云端投资知识库</h4>
              <p>全员参与的方式沉淀公司文档和知识，让投资随时随地查阅产品信息，报价，成功案例，合同等，提升效率和项目赢率。</p>
              <a href="p/cooperate/">了解详细&gt;&gt;</a></div>
            <div class="content_d">
              <h4 class="intr_04">
                移动办公</h4>
              <p>通过手机，随时随地查看业绩进展，处理折扣申请，审批合同，分派任务，无需再授权秘书或耽搁业务，尽享最新PEPM管理理念。</p>
              <a href="p/mobile-office/">了解详细&gt;&gt;</a></div>
            <div class="content_d">
              <h4 class="intr_05">云端管理</h4>
              <p>基于云端的服务，引领PEPM云计算浪潮，无需安装任何软硬件，打开浏览器或手机即可使用，系统永远使用最新版本。</p>
              <a href="p/cloud-compute/">了解详细&gt;&gt;</a></div>
            <div class="content_d">
              <h4 class="intr_06">
                企业级安全</h4>
              <p>
                不安全，一切都是空谈，PEPM部署在Amazon EC2，实时数据备份，定期冗灾备份；严格的内部安全机制；保密协议的商务保障；SSL传输加密协议技术，数字安全证书。</p>
              <a href="p/security/">了解详细&gt;&gt;</a></div>
 
    <div class="clr"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="clr"></div>
  </div>

    
    
    <div id="sp-rightcol" class="clearfix">
    
      <ul id="feature_text">    <li><h4>主要功能模块</h4></li>
    <li><span class="glyphicons-icon iphone"></span>完美支持移动办公</li>
    <li><span class="glyphicons-icon wifi_alt"></span>舆情监控</li>
    <li><span class="glyphicons-icon show_thumbnails"></span>项目池管理分析</li>
    <li><span class="glyphicons-icon usd"></span>募资管理及CRM</li>
    <li><span class="glyphicons-icon stats"></span>自动计算IRR及倍数</li>
    <li><span class="glyphicons-icon folder_open"></span>史上最强文档管理</li>
    <li><span class="glyphicons-icon check"></span>任务管理，项目跟踪</li>
    <li><span class="glyphicons-icon user"></span>LP关系管理</li>
    <li><span class="glyphicons-icon pie_chart"></span>自动化图表分析</li>
    <li><span class="glyphicons-icon settings"></span>高度可定制</li>
    <li><h4>联系我们</h4></li>
    <li><span class="glyphicons-icon phone_alt"></span> 400-626-5464 </li>
    <li><span class="glyphicons-icon envelope"></span> info@pepm.com.cn</li>
  </ul>
  </div>
</div>
<!--Module Position breadcrumbs-->
<div class="clr"></div>
<div class="custom logo_list_area">
  <div class="logo_list"> </div>
</div>
