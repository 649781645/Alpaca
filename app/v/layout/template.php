<!DOCTYPE html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title><?=$page_title?$page_title:$title?></title>
  <meta name="keywords" content="<?=$meta_keywords?>"/>
  <meta name="description" content="<?=$meta_description?>"/>
  <link href="static/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="static/basic.css" rel="stylesheet" type="text/css">
</head>
<body>
  <div id="wrap">
  <div id="header" >
    <h1><?=alpa('sitename')?></h1>
    <h2><?=alpa('subsitename')?></h2>
  </div>
  <ul id="menu" ><?=alpa('menu')?></ul>
  <div id="content" >
    <?=$al_content?>
  </div>
  <div id="footer" ><a href="http://alpaca.b24.cn/" >羊驼</a> 提供澎湃动力</div>
  </div>
</body>
</html>
