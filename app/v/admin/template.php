<!DOCTYPE html>
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title><?=$page_title?></title>
  <meta name="keywords" content="<?=$meta_keywords?>"/>
  <meta name="description" content="<?=$meta_description?>"/>
  <link href="static/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <style>
  #nav{padding:10px 0 0 70px;background:#a00 url(static/yang_logo.png) 10px 0 no-repeat;border-radius:3px;margin:5px 0 20px;}
  #nav a{color:#fff;}
  #nav a:hover{color:#444;}
  #nav .active a{color:#333;}
  </style>
  <script charset="utf-8" src="static/js/jquery.js"></script>
  <script charset="utf-8" src="static/js/alpaca.js"></script>
  <script charset="utf-8" src="static/js/bootstrap.min.js"></script>
</head>
<body>
  <div class="container">
    <ul class="nav nav-tabs" id="nav" >
      <?foreach($menu as $url=>$m) {?>
      <li <?=($url==seg(2))?'class="active"':''?> ><a href="<?=(($url == 'logout') ? '?/':'?/admin/').$url?>/"><?=$m?></a></li>
      <?}?>
    </ul>
    
    <?=$al_content?>
  </div>
</body>
</html>
