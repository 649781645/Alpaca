<script charset="utf-8" src="static/kindeditor-min.js"></script>
<script>
  var editor;
  KindEditor.ready(function(K) {
    editor = K.create('textarea[name="content"]', {
      resizeType : 1,   
      cssPath : 'static/pepm.css',
      width:700,
      allowPreviewEmoticons : false,
      allowImageUpload : false,
    });
  });
</script>

<ul class="breadcrumb">
  <li><a href="?/admin/page/">首页</a> <span class="divider">/</span></li>
  <li><a href="?/admin/page/<?=$uper['id']?>/"><?=$uper['title']?></a> </li>
</ul>


<form class="form-horizontal" method="POST" >
  <div class="control-group">
    <label class="control-label" >页面名称</label>
    <div class="controls">
      <input type="text" name="name" required value="<?=$page['name']?>" >
      <input type="hidden" name="type" value="page" required >
      <input type="hidden" name="upid" value="<?=$page['upid']?>" >
    </div>
  </div>

  <div class="control-group">
    <label class="control-label" >页面标题</label>
    <div class="controls">
      <input type="text" class="input-xxlarge" name="title" value="<?=$page['title']?>" required >
    </div>
  </div>
  
  <div class="control-group">
    <label class="control-label" >网页内容</label>
    <div class="controls">
      <textarea name="content" rows="20" ><?=htmlspecialchars($page['content'])?></textarea>
    </div>
  </div>
  
  <div class="control-group">
    <label class="control-label" >扩展字段</label>
    <div class="controls">
      <textarea name="ext" rows="3" class="input-xxlarge" ><?=htmlspecialchars($page['ext'])?></textarea>
    </div>
  </div>

  <?foreach($exts as $ext=> $label ){?>
  <div class="control-group">
    <label class="control-label" ><?=$label?></label>
    <div class="controls">
      <input type="text" class="input-xxlarge" name="ext1[<?=$ext?>]" value="<?=$ext1[$ext]?>" />
    </div>
  </div>
  <?}?>
  
  <div class="control-group">
    <label class="control-label" >风格文件</label>
    <div class="controls">
      <select type="text" class="input-xxlarge" name="ext1[template]"  >
        <option></option>
        <?foreach($layouts as $layout){?>
        <option  <?=$ext1['template']==$layout?'selected':''?> ><?=$layout?></option>
        <?}?>
      </select>
    </div>
  </div>
  
  <div class="control-group">
    <label class="control-label" >排版文件</label>
    <div class="controls">
      <select type="text" class="input-xxlarge" name="ext1[layout]"  >
        <option></option>
        <?foreach($layouts as $layout){?>
        <option  <?=$ext1['layout']==$layout?'selected':''?> ><?=$layout?></option>
        <?}?>
      </select>
    </div>
  </div>
  
  <div class="form-actions">
    <button type="submit" class="btn btn-primary">确认提交</button>
  </div>
</form>
