<a href="?/admin/page/add/<?=$upid?>/" class="pull-right btn" ><i class="icon-plus"></i></a>
<ul class="breadcrumb">
  <li><a href="?/admin/page/">首页</a> <span class="divider">/</span></li>
  <li class="active" ><?=$uper['title']?></li>
</ul>

<table class="table">
  <thead>
    <tr>
      <th width="5" >#</th><th>标题</th><th>页面</th><th width="100" >更新日期</th><th width="120" >操作</th>
    </tr>
  </thead>
  <?foreach($records as $r){?>
  <tr id="r<?=$r['id']?>" title="line<?=++$i?>">
    <td><input type="checkbox" ></td><td><?=$r['title']?></td><td><?=$r['name']?></td>
    <td><?=date('Y-m-d',$r['update_date'])?></td>
    <td>
      <a href="javascript:order_up(<?=$r['id']?>);"><i class="icon-arrow-up"></i></a>
      <a href="javascript:order_down(<?=$r['id']?>);"><i class="icon-arrow-down"></i></a>
      <a href="?/home/page/<?=$r['name']?>/" target="_blank" ><i class="icon-search"></i></a>
      <a href="?/admin/page/<?=$r['id']?>/" ><i class="icon-folder-open"></i></a>
      <a href="?/admin/page/edit/<?=$r['id']?>/" ><i class="icon-edit"></i></a>
      <a href="javascript:bdel('?/admin/page/del/<?=$r['id']?>/', 'r<?=$r['id']?>');" ><i class="icon-trash"></i></a>
    </td>
  </tr>
  <?}?>
</table>

<script>
function order_up(id) {
  line = $('#r' + id).attr('title');
  lineno = line.substring(line.length - 1);
  if(lineno == 1) {
    alert('本文章已经为第一篇！');
  } else {
    $.get('?/admin/order_edit/' + id + '/up', function(data){
      elem_change('line' + lineno, 'line' + (parseInt(lineno) - 1));
    });
  }
}

function order_down(id) {
  line = $('#r' + id).attr('title');
  lineno = line.substring(line.length - 1);
  if(lineno == <?=$i?>) {
    alert('本文章已经为最后一篇！');
  } else {
    $.get('?/admin/order_edit/' + id + '/down', function(data){
      elem_change('line' + (parseInt(lineno) + 1), 'line' + lineno);
    });
  }
}

function elem_change(elem1, elem2) {
  line1 = $('tr[title='+elem1+']');
  id1 = line1.attr('id');
  line2 = $('tr[title='+elem2+']');
  id2 = line2.attr('id');
  temp = line1.html();
  line1.html(line2.html());
  line2.html(temp);
  line1.attr('id', id2);
  line2.attr('id', id1);
}
</script>