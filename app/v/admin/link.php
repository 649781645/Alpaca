<a href="?/admin/link/add/" class="pull-right btn" ><i class="icon-plus"></i></a>

<table class="table">
  <thead>
    <tr>
      <th width="5" >#</th><th width="50%" >名称</th><th>更新日期</th><th>操作</th>
    </tr>
  </thead>
  <?foreach($records as $r){?>
  <tr id="r<?=$r['id']?>">
    <td><input type="checkbox" ></td><td><?=$r['name']?></td>
    <td><?=date('Y-m-d',$r['update_date'])?></td>
    <td>
      <a href="?/admin/link/edit/<?=$r['id']?>/" ><i class="icon-edit"></i></a>
      <a href="javascript:bdel('?/admin/link/del/<?=$r['id']?>/', 'r<?=$r['id']?>');" ><i class="icon-trash"></i></a>
    </td>
  </tr>
  <?}?>
</table>
