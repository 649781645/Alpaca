<?php
// 所有配置内容都可以在这个文件维护
error_reporting(E_ERROR);
//error_reporting(E_ALL);

define('ALPA_VERSION', '4.0');
// 配置url路由
$route_config = array(
  '/login/'=>'/user/login/',
  '/reg/'=>'/user/reg/',
  '/logout/'=>'/user/logout/',
  '/p/'=>'/home/page/',
);

if(file_exists(APP.'config_user.php')) require(APP.'config_user.php');
if(file_exists(APP.'config_app.php')) require(APP.'config_app.php');
